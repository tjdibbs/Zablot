import {makeStyles} from "@mui/styles";

const Styles = makeStyles({
	label: {
		"& .MuiInputLabel-root": {
			fontSize: 18,
			fontFamily: "calibri",
		},
	},
});

export default Styles;
